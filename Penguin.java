class Penguin extends Bird {
    
/* Properties */
    int dive_depth;


    /* Constructor */
    // Constructor with all properties
    Penguin(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, int num_of_toes, int length_of_claws, boolean can_fly, int wing_size, String beak_type, int dive_depth){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs, num_of_toes, length_of_claws, can_fly, wing_size, beak_type);
        this.dive_depth = dive_depth;
    }

    // Constructor for user-input additions
    Penguin(String name){
        super(name);
    }

    /* Methods */
    String feedAnimal(){
        return "krill, squids, and fishes";
    };
    
    String prepareEnclosure(){
        return "islands, oceans, and coasts";
    }


    // Set methods
    void setDiveDepth(int dive_depth){
        this.dive_depth = dive_depth;
    }

    // Get methods
    int getDiveDepth(){
        return this.dive_depth;
    }


}