abstract public class Mammal extends Animal {

    /* Properties */
    int fur_length;
    String fur_color;
    int mane_length;

    /* Constructor */
    // Constructor with all properties
    Mammal(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, int num_of_toes, int length_of_claws, int fur_length, String fur_color, int mane_length){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs, num_of_toes, length_of_claws);
        this.fur_length = fur_length;
        this.fur_color = fur_color;
        this.mane_length = mane_length;
    }

    // Constructor for mammals without toes or claws
     Mammal(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, int fur_length, String fur_color, int mane_length){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs);
        this.fur_length = fur_length;
        this.fur_color = fur_color;
        this.mane_length = mane_length;
    }

    // Constructor for user-input additions
     Mammal(String name){
        super(name);
     }

    /* Methods */
    // Set methods
    void setFurColor(String fur_color){
        this.fur_color = fur_color;
    }
    void setFurLength(int fur_length){
        this.fur_length = fur_length;
    }

    void setManeLen(int mane_length){
        this.mane_length = mane_length;
    }

    // Get methods
    int getFurLength(){
        return this.fur_length;
    }

    String getFurColor(){
        return this.fur_color;
    }

    int getManeLen(){
        return this.mane_length;
    }



}