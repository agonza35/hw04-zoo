class Shark extends Fish {


    /* Properties */
    boolean glow_in_dark;

    /* Constructor */
    // Constructors with all properties
    Shark(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, String water_type, String fin_shape, String scale_color, boolean glow_in_dark){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs, water_type, fin_shape, scale_color);
        this.glow_in_dark = glow_in_dark;
    }

    // Constructor for user-input additions
    Shark(String name){
        super(name);
    }

    /* Methods */
    String feedAnimal(){
        return "shrimp, fish, squid and crustaceans";
    };
    
    String prepareEnclosure(){
        return "deep sea environments";
    }
    // Set methods
    void setGlow(boolean glow_in_dark){
        this.glow_in_dark = glow_in_dark;
    }

    // Get methods
    boolean getGlow(){
        return this.glow_in_dark;
    };




}