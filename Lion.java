public class Lion extends Mammal {
    //Properties 
    int paw_size;
    
    /* Constructor */
    // Constructor with all properties
    Lion(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, int num_of_toes, int length_of_claws, int fur_length, String fur_color, int mane_length, int paw_size){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs, num_of_toes, length_of_claws, fur_length, fur_color, mane_length);
        this.paw_size = paw_size;
    }

    // Constructor for user-input additions
    Lion(String name){
        super(name);
    }


 

    /* Methods */
    String feedAnimal(){
        return "medium and large hoofed animals";
    };
    
    String prepareEnclosure(){
        return "open plains and thick brush";
    }


    // Set methods
     void setPawSize(int paw_size){
        this.paw_size = paw_size;
    }


    // Get methods 
    int getPawSize(){
        return this.paw_size;
    }

}