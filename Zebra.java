public class Zebra extends Mammal {
    //Properties 
    int num_of_stripes;
    int hoof_size;

    /* Constructor */
    // Constructor with all properties
    Zebra(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, int fur_length, String fur_color, int mane_length, int num_of_stripes, int hoof_size){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs, fur_length, fur_color, mane_length);
        this.num_of_stripes = num_of_stripes;
        this.hoof_size = hoof_size;
    }

    // Constructor for user-input additions
    Zebra(String name){
        super(name);
    }

    /* Methods */
    String feedAnimal(){
        return "grasses (herbivore)";
    };
    
    String prepareEnclosure(){
        return "treeless grasslands";
    }

    // Set methods
    void setNumStripes(int num_of_stripes){
        this.num_of_stripes = num_of_stripes;
    }
    void setHoofSize(int hoof_size){
        this.hoof_size = hoof_size;
    }

    // Get methods 
    int getNumStripes(){
        return this.num_of_stripes;
    }
    int getHoofSize(){
        return this.hoof_size;
    }

}