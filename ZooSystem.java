import java.lang.*;
import java.util.*; //used ArrayList in order to use .add(), .getClass() , .remove() and .size() 
import java.util.Scanner; // used for input 

public class ZooSystem { 
    public static ArrayList<Animal> setupAnimals(){
    // create the zoo list of animals
        ArrayList<Animal> alist = new ArrayList<Animal>(7);
        alist.add(new Lion("Fiona", 5, "once a year", 4, 3, true,  4, 10, 2, 3,"Brown", 4, 3));
        alist.add(new Zebra("Stripes", 7,"once every two years", 5, 3, true, 4, 3, "Black and white stripes", 2, 45, 6));
        alist.add(new Shark("Jaws", 4, "every two to three years", 1, 3, false, 2, "saltwater", "dorsal", "grey", false));
        alist.add(new Clownfish("Nemo", 2, "100 eggs a year", 7, 6, false , 1, "salt water", "trapezoidal but rounded", "orange, black and white",  "has returned to anemone", true));
        alist.add(new Parrot("Larry", 2, "2-4 eggs a year ", 5, 7, true, 1, 4, 1, true, 1,"hookbills" , true, "Pretty bird"));
        alist.add(new Penguin("Happy", 2, "once every year and a half", 7, 7, true, 1, 3, 1, false, 1, "long bill", 50));
        alist.add(new Penguin("Pat", 2, "once every year and a half", 7, 7, true, 1, 3, 1, false, 1, "long bill", 50));
        System.out.println("\n");
        
        return alist;
    }	

    //Prints number of each species 
    static void printSummaryView(ArrayList<Animal> alist){
        /* Keeps track of how many of each animal species there are */
        int numLion = 0;
        int numZebra = 0;
        int numClownfish = 0;
        int numShark = 0;
        int numParrot = 0;
        int numPenguin = 0;
         
        /*Iterates through list of animals and checks how many of each species there are*/ 
        for (int i = 0; i < alist.size(); i++) {
            if (alist.get(i).getClass().toString().split(" ")[1].toLowerCase().equals("lion")){
                numLion += 1;
            }
            else if (alist.get(i).getClass().toString().split(" ")[1].toLowerCase().equals("zebra")){
                numZebra += 1;
            }
            else if (alist.get(i).getClass().toString().split(" ")[1].toLowerCase().equals("clownfish")){
                numClownfish += 1;
            }
            else if (alist.get(i).getClass().toString().split(" ")[1].toLowerCase().equals("shark")){
                numShark += 1;
            }
            else if (alist.get(i).getClass().toString().split(" ")[1].toLowerCase().equals("parrot")){
                numParrot += 1;
            }
            else if (alist.get(i).getClass().toString().split(" ")[1].toLowerCase().equals("penguin")){
                numPenguin += 1;
            }
            
        }

        System.out.println("\n");
        System.out.println("Summary: ");
        System.out.println("There are " + alist.size() + " animals in the zoo.");
        
        //Conditionals to make sure that only the number of species that are in the zoo are printed
        if(numLion != 0){
            System.out.println(numLion + " Lions(s)");
        }
        if(numZebra != 0){
            System.out.println(numZebra + " Zebra(s)");
        }
        if(numClownfish != 0){
            System.out.println(numClownfish + " Clownfish(s)");
        }
        if(numShark != 0){
            System.out.println(numShark + " Shark(s)");
        }
        if(numParrot != 0){
            System.out.println(numParrot + " Parrot(s)");
        }
        if(numPenguin != 0){
            System.out.println(numPenguin + " Penguin(s)");
        }
        
    } // end of summary view

    // Prints out list of all the animals in the zoo (name and the species) in order 
    public static void printVerboseList(ArrayList<Animal> alist){ 
        System.out.println("\n");
        System.out.println("Verbose List of Animals with details:");
        System.out.println("\tName\tSpecies");
        for (int i = 0;i < alist.size(); i++){
          //  System.out.println(alist.get(i).getClass().toString().split(" ")[1]);
            System.out.println(i+1 + ":\t" + (alist.get(i)).getName() + "\t" + (alist.get(i).getClass().toString().split(" ")[1]));
        }
        System.out.println("\n");
    } // end of printVerboseList

    //Prints out options user can input 
    static void printInteractiveOptions(){
        System.out.println("\n");
        System.out.println("Type exit at any time to end the program");
        System.out.println("Options:");
        System.out.println("   Add Animal : type 'add'");
        System.out.println("   Delete Animal : type 'delete' ");
        System.out.println("   Display Animal : type 'display");
        System.out.println("\n");
        
    } // end of printInteractactiveOptions

    //checks option that is selected and calls corresponding function or does action indicated
    static ArrayList<Animal> checkAnimalInput(String input, ArrayList <Animal> alist){
        //adds animal to the zoo via name 
        if (input.equals("add")){
            System.out.println("\n");
            System.out.println("What animal would you like to add to the zoo? ");
            Scanner in = new Scanner(System.in);
            String species = in.next().toString();
            String name = in.next().toString();
            Animal a;


            //Checks what animal species was input and creates a new instance of that animal to eventually add to the Array List of animals
            if (species.toLowerCase().equals("lion")){
                a = new Lion(name);
            }
            else if (species.toLowerCase().equals("zebra")) {
                a = new Zebra(name);
            }
            else if (species.toLowerCase().equals("parrot")){
                a = new Parrot(name);
            }
            else if (species.toLowerCase().equals("penguin")) {
                a = new Penguin(name);
            }
            else if (species.toLowerCase().equals("clownfish")) {
                a = new Clownfish(name);
            }
            else if (species.toLowerCase().equals("shark")){
                a = new Shark(name);
            }
            else{
                System.out.println("\n");
                System.err.println("Animal not recognized. Please enter another.");
                return alist;
            }
            
            return addAnimal(a, alist);
        }

        //deletes animal from zoo via name  (same functionality as deleteAnimal() method)
        if (input.equals("delete")){
            System.out.println("\n");
            System.out.println("What animal would you like to delete from the zoo (name)?");
            Scanner in = new Scanner(System.in);
            String name = in.next().toString();
            
            // iterates through list of animals until it finds 
            for (int i = 0; i < alist.size(); i++){
                if (alist.get(i).getName().toLowerCase().equals(name.toLowerCase())){
                    alist.remove(i);
                    return alist;
                } else {
                    continue;
                }
            }
            System.out.println("\n");
            System.out.println("Animal not recognized");
            return alist;
        }
        if (input.equals("display")){
            System.out.println("\n");
            System.out.println("What animal would you like to display (name)?");
            Scanner in = new Scanner(System.in);
            String name = in.next().toString();
            System.out.println("\n");
            for (int i = 0; i < alist.size(); i++){
                if (alist.get(i).getName().toLowerCase().equals(name.toLowerCase())){
                    String diet = alist.get(i).feedAnimal();
                    System.out.print(alist.get(i).getName() + " prefers to eat " + diet + ". ");
                    String habitat = alist.get(i).prepareEnclosure();
                    System.out.println(alist.get(i).getName() + " lives in " + habitat + ".");
                    return alist;
                } else {
                    continue;
                }
            }
            System.out.println("\n");
            System.out.println(name + " not recognized");
            return alist;
            
        }
        return alist;
    } // end of check animal input


    // Adds an animal to the zoo 
    static ArrayList <Animal> addAnimal(Animal a, ArrayList<Animal> alist){
        alist.add(a);
        System.out.println("\n");
        System.out.println("Name: " + a.getName());
        System.out.println("Species: " + a.getClass().toString().split(" ")[1]);;
        System.out.println("Friendliness: " + a.getFriendlinessScore());
        return alist;
    } 

    //Main function 
    public static void main(String[]args) {
         //TODO	interactively take input from user to manage zoo.
        ArrayList <Animal> alist = setupAnimals();
        printSummaryView(alist);
        printVerboseList(alist);

        String input = "";
        while (!(input.equals("exit"))){
            printInteractiveOptions();
            System.out.print("Enter Selection: ");
            Scanner in = new Scanner(System.in);
            input = in.next().toString();
            alist = checkAnimalInput(input, alist);
    
            if (!(input.equals("exit"))) printSummaryView(alist);
        }
        
    } // end of main function

}// end of ZooSystem Class
