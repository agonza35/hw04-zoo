class Parrot extends Bird {

    /* Properties */
    boolean can_repeat;
    String repeatPhrase;


    /* Constructor */
    // Constructor with all properties
    Parrot(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, int num_of_toes, int length_of_claws, boolean can_fly, int wing_size, String beak_type, boolean can_repeat, String repeatPhrase){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs, num_of_toes, length_of_claws, can_fly, wing_size, beak_type);
        this.can_repeat = can_repeat;
        this.repeatPhrase = repeatPhrase;
    }

    // Constructor for user-input additions
    Parrot(String name){ 
        super(name);
    }

    /* Methods */
    String feedAnimal(){
        return "fresh fruits, vegetables, seeds, and nuts";
    };
    
    String prepareEnclosure(){
        return "woodlands or rainforests";
    }
    // Set methods
    void setCanRepeat(boolean can_repeat){
        this.can_repeat = can_repeat;
    }
    void setRepeatPhrase(String repeatPhrase){
        this.repeatPhrase = repeatPhrase;
    }
    
    // Get methods
    boolean getCanRepeat(){
        return this.can_repeat;
    }
    String getRepeatPhrase(){
        return this.repeatPhrase;
    }
   

}