public class Clownfish extends Fish {
    //Properties 
    String return_to_anemone;
    boolean immunity_acquired;

    /* Constructor */
    // Constuctor with all properties
    Clownfish(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, String water_type, String fin_shape, String scale_color,  String return_to_anemone, boolean immunity_acquired){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs, water_type, fin_shape, scale_color);
        this.return_to_anemone = return_to_anemone;
        this.immunity_acquired = immunity_acquired;
    }

    // Constructor for user-input additions
     Clownfish(String name){
        super(name);
    }

    /* Methods */
    String feedAnimal(){
        return "algae and various small invertebrates";
    }
    
    String prepareEnclosure(){
        return "warm waters, anemone, or sheltered reefs";
    }

    // Set methods
    void setAnemone(String return_to_anemone){
        this.return_to_anemone = return_to_anemone;
    }
    void setImmunity(boolean immunity_acquired){
        this.immunity_acquired = immunity_acquired;
    }

    // Get methods 
    String getAnemone(){
        return this.return_to_anemone;
    }

    boolean getImmunity(){
        return this.immunity_acquired;
    }
    
   

}