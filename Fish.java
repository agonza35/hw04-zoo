abstract public class Fish extends Animal {
    /* Properties */
    String water_type;
    String fin_shape;
    String scale_color;

    /* Constructor */
    // Constructor with all properties
    Fish(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, String water_type, String fin_shape, String scale_color){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs);
        this.water_type = water_type;
        this.fin_shape = fin_shape;
        this.scale_color = scale_color;
    }

    // Constructor for user-input additions
    Fish(String name){
        super(name);
    }


    /* Methods */
    
    // Set method
    void setWaterType(String water_type){
        this.water_type = water_type;
    }

    void setFinShape(String fin_shape){
        this.fin_shape = fin_shape;
    }

    void setScaleColor(String scale_color){
        this.scale_color = scale_color;
    }

    // Get method
    String getWaterType(){
        return this.water_type;
    }
    String getFinShape(){
        return this.fin_shape;
    }

    String getScaleColor(){
        return this.scale_color;
    }







}