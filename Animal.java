abstract public class Animal{

    //Properties 
    String name;
    int age; 
    String frequency_of_babies;
    int safety_score;
    int friendliness_score;
    boolean warm_blooded;
    int size_of_eggs;
    int num_of_toes;
    int length_of_claws;


    /* Constructor */
    // Constructor with all properties
    Animal(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, int num_of_toes, int length_of_claws){
        this.name = name;
        this.age = age; 
        this.frequency_of_babies = frequency_of_babies;
        this.safety_score = safety_score;
        this.friendliness_score = friendliness_score;
        this.warm_blooded = warm_blooded;   
        this.size_of_eggs = size_of_eggs;
        this.num_of_toes = num_of_toes;
        this.length_of_claws = length_of_claws;
    }

    // Constructor for animals without claws or toes 
    Animal(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs){
        this.name = name;
        this.age = age; 
        this.frequency_of_babies = frequency_of_babies;
        this.safety_score = safety_score;
        this.friendliness_score = friendliness_score;
        this.warm_blooded = warm_blooded;   
        this.size_of_eggs = size_of_eggs;
    }

    // Constructor for user-input
     Animal(String name){
        this.name = name;
        this.friendliness_score = 5;
    }
    
    /* Methods */
    abstract String feedAnimal();
    abstract String prepareEnclosure();

    // Set methods
    void setName(String name){
        this.name = name;
    }
    void setAge(int age){
        this.age = age;
    }
    void setFrequencyofBabies(String frequency_of_babies){
        this.frequency_of_babies = frequency_of_babies;
    }
    void setSafetyScore(int safety_score){
        this.safety_score = safety_score;
    }
    void setFriendlinessScore(int friendliness_score){
        this.friendliness_score = friendliness_score;
    }
    void setBloodType(boolean warm_blooded){
        this.warm_blooded = warm_blooded;
    }
    void setSizeEggs(int size_of_eggs){
        this.size_of_eggs = size_of_eggs;
    }
    void setNumToes(int num_of_toes){
        this.num_of_toes = num_of_toes;
    }
    void setLenClaws(int setLenClaws){
        this.length_of_claws = length_of_claws;
    }

    // Get methods 
    final String getName(){
        return this.name;
    }
    int getAge(){
        return this.age;
    }
    String getFrequencyofBabies(){
        return this.frequency_of_babies;
    }
    int getSafetyScore(){
        return this.safety_score;
    }
    int getFriendlinessScore(){
        return this.friendliness_score;
    }
    boolean getBloodType(){
        return this.warm_blooded;
    }
    int getSizeEggs(){
        return this.size_of_eggs;
    }
    int getNumToes(){
        return this.num_of_toes;
    }
    int getLenClaws(){
        return this.length_of_claws;
    }

}