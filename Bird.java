abstract public class Bird extends Animal {

    /* Properties */
    boolean can_fly;
    int wing_size;
    String beak_type;


    /* Constructor */
    // Constructor with all properties
    Bird(String name, int age, String frequency_of_babies, int safety_score, int friendliness_score, boolean warm_blooded, int size_of_eggs, int num_of_toes, int length_of_claws, boolean can_fly, int wing_size, String beak_type){
        super(name, age, frequency_of_babies, safety_score, friendliness_score, warm_blooded, size_of_eggs, num_of_toes, length_of_claws);
        this.can_fly = can_fly;
        this.wing_size = wing_size;
        this.beak_type = beak_type;
    }

    // Constructor for user-input additions
    Bird(String name){
        super(name);
     }
    

    
    /* Methods */

    // Set methods:
    void setCanFly(boolean can_fly){
        this.can_fly = can_fly;
    }

    void setWingSize(int wing_size){
        this.wing_size = wing_size;
    }

    void setBeakType(String beak_type){
        this.beak_type = beak_type;
    }

    // Get methods
    boolean getCanFly(){
        return this.can_fly;
    }

    int getWingSize(){
        return this.wing_size;
    }

    String getBeakType(){
        return this.beak_type;
    }



}